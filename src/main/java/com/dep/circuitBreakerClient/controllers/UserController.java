package com.dep.circuitBreakerClient.controllers;

import com.dep.circuitBreakerClient.models.User;
import com.dep.circuitBreakerClient.services.UserService;
import com.dep.circuitBreakerClient.services.UserServiceWithoutCircuitBreaker;
import reactor.core.publisher.Mono;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class UserController {

    private final UserService userService;
    private final UserServiceWithoutCircuitBreaker userServiceWithoutCircuitBreaker;

    @Autowired
    public UserController(UserService userService, UserServiceWithoutCircuitBreaker userServiceWithoutCircuitBreaker){
        this.userService = userService;
        this.userServiceWithoutCircuitBreaker = userServiceWithoutCircuitBreaker;
    }

    @GetMapping("/user-data")
    public Mono<User> getUserDataWithCircuitBreaker() {
        return userService.getUserData();
    }

    @GetMapping("/user-data-without-cb")
    public Mono<User> getUserDataWithoutCircuitBreaker() {
        return userServiceWithoutCircuitBreaker.getUserData();
    }
}
