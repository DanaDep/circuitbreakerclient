package com.dep.circuitBreakerClient.models;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastname;
	@JsonProperty
	private String email;
}
