package com.dep.circuitBreakerClient.services;

import com.dep.circuitBreakerClient.models.User;
import reactor.core.publisher.Mono;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

	private final WebClient webClient;
	private final ReactiveCircuitBreaker circuitBreaker;

	private static int counter = 0;

	@Autowired
    public UserService(ReactiveCircuitBreakerFactory<?, ?>   circuitBreakerFactory) {
		this.webClient = WebClient.builder()
				.baseUrl("http://localhost:8080")
				.build();
        this.circuitBreaker = circuitBreakerFactory.create( "dep" );
    }

    public Mono<User> getUserData() {
		LOG.info("Getting user data from server");
		LOG.info( "Call number: " + ++counter);
        return circuitBreaker.run(webClient.get()
                .uri("/server/user-data")
                .retrieve()
                .bodyToMono(User.class), throwable -> {
            LOG.warn("Server error: " + throwable.getMessage());
            return Mono.just(new User());
        });
    }
}
