package com.dep.circuitBreakerClient.services;
import com.dep.circuitBreakerClient.models.User;
import reactor.core.publisher.Mono;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class UserServiceWithoutCircuitBreaker {
	private final WebClient webClient;

	public UserServiceWithoutCircuitBreaker() {
		this.webClient = WebClient.builder()
				.baseUrl("http://localhost:8080")
				.build();
	}

	public Mono<User> getUserData() {
		return webClient.get()
				.uri("/server/user-data")
				.retrieve()
				.onStatus(
						status -> status.is4xxClientError() || status.is5xxServerError(),
						clientResponse -> Mono.error(new Exception("Error during getting user data from server"))
				)
				.bodyToMono(User.class)
				.onErrorResume(Exception.class, e -> Mono.just(new User()) );
	}
}
